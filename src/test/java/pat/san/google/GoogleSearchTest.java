package pat.san.google;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GoogleSearchTest extends GoogleBaseTest {

    @Test
    public void searchTest() {
        assertThat(googleMainPage.getTitle()).as("Invalid page title").isEqualTo("Google");
        googleMainPage.searchFor("samochód");
        googleMainPage.waitForResultsPage();
        assertThat(googleMainPage.getSearchResultsCount()).as("Invalid results count").isGreaterThan(6);
    }
}
