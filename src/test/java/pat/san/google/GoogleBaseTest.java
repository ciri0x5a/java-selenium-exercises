package pat.san.google;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import pat.san.util.DriverProvider;

public abstract class GoogleBaseTest {

    private static final String APP_URL = "http://google.com";
    protected GoogleMainPage googleMainPage;

    @BeforeSuite
    public void setUp() {
        WebDriver driver = DriverProvider.getInstance().getDriver();
        driver.get(APP_URL);
        googleMainPage = new GoogleMainPage();
    }

    @AfterSuite
    public void tearDown() {
        DriverProvider.getInstance().shutDown();
    }
}
