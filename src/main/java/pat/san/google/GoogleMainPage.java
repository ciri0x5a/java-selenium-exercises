package pat.san.google;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pat.san.BasePage;

public class GoogleMainPage extends BasePage {

    private static final By queryBoxSelector = By.name("q");
    private static final By resultStatsSelector = By.id("result-stats");
    private static final By resultLinkSelector = By.xpath("(//div[@class='r']/a)");

    public void searchFor(String text) {
        WebElement queryBoxElement = getDriver().findElement(queryBoxSelector);
        queryBoxElement.sendKeys(text);
        queryBoxElement.submit();
    }

    public void waitForResultsPage() {
        getWait(10).until(ExpectedConditions.visibilityOfElementLocated(resultStatsSelector));
    }

    public int getSearchResultsCount() {

        return getDriver().findElements(resultLinkSelector).size();
    }
}
