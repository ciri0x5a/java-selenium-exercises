package pat.san;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pat.san.util.DriverProvider;

public abstract class BasePage {

    public String getTitle() {

        return getDriver().getTitle();
    }

    protected WebDriver getDriver() {

        return DriverProvider.getInstance().getDriver();
    }

    protected WebDriverWait getWait(int timeout) {

        return new WebDriverWait(getDriver(), timeout);
    }

}
