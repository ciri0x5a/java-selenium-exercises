package pat.san.util;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * This class is only POC - in real life you would use RemoteDriver
 * and dockers: hub and selenium nodes with preferred browsers
 */
public class DriverProvider {

    private static DriverProvider instance;

    private WebDriver driver;

    private DriverProvider() {
    }

    public static DriverProvider getInstance() {
        if (instance == null) {
            instance = new DriverProvider();
        }

        return instance;
    }

    public WebDriver getDriver() {
        if (driver == null) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver(getOptions());
            driver.manage().window().maximize();
        }

        return driver;
    }

    public void shutDown() {
        driver.quit();
    }

    /**
     * Long story short: "Ever Growing List of Useless Arguments" - https://stackoverflow.com/a/52340526
     *
     * @return prepared options for Chrome Browser
     */
    private ChromeOptions getOptions() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("enable-automation");
//        options.addArguments("--headless");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-infobars");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-browser-side-navigation");
        options.addArguments("--disable-gpu");

        return options;
    }
}
